+++
title = "Jan Amborski"
+++

Absolwent Staszica, były **MATEX**. Potem Wydział Mechaniczny Energetyki i Lotnictwa, po przeciwnej niż Staszic stronie skrzyżowania ulicy Nowowiejskiej i alei Nipodległości. Następnie, w styczniu 2006, obronił doktorat na Wydziale Samochodów i Maszyn Roboczych Politechniki Warszawskiej .

Fotografią zainteresował się w liceum. W zasadzie z konieczności. Egzamin z fotografii był jednym z koniecznych do zdobycia Złotej Odznaki Pałacu Młodzieży w Warszawie. Uczęszczał do pracowni fotografii Pałacu, prowadzonej przez Małgorzatę Mikołajczyk i Annę Białą, gdzie niedługo potem został asystentem. W tym też czasie dołączył do fotograficznej grupy artystycznej "RAMA". Grupa miała liczne wernisaże, z których najciekawszymi były: w Galerii Przyjaciół Akademii Ruchu "AKT" i w Areszcie Śledczym przy ulicy Rakowieckiej. Na ten drugi zostali zaproszeni jedynie więźniowie z wyrokami powyżej 10 lat. Był to pierwszy wernisaż w tym więzieniu. Kolejne lata to spotkania ze znakomitościami polskiej fotografiki: Ryszardem Horowitzem i Edwardem Hartwigiem oraz kursy fotografii wielkoformatowej organizowane przez firmę SINAR przy ASP.

Następnym etapem fotograficznej przygody była praca zarobkowa. Na początku zdjęcia dla magazynów motoryzacyjnych. Potem wielkie wyzwanie - praca jako fotograf w klubie Remont, klubie w którym przedtem pracował słynny Marek Karewicz. Tam miał liczne wystawy, m. in. projekt fotograficzny "13 XII w muzyce alternatywnej" - zdjęcia barwione, płynnie przechodzące jedne w drugie. Jednocześnie rozpoczął trwającą do dziś współpracę z agencją fotograficzną "DELTA Press".

