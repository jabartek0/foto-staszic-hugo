+++
title = "Nowa strona Kółka!"
Date = "2019-11-27T01:00:00+01:00"
author = "Bartłomiej Janowski"
cover = ""
tags = ["strona","www","inne"]
description = ""
showFullContent = true
+++

Po paru dniach zgłębiania przeze mnie wiedzy w zakresie języka [Go](https://golang.org/) oraz frameworka webowego [Hugo](https://gohugo.io/) w końcu powstała Ona -- nowa strona naszego kółka! Tak, ta na którą właśnie patrzycie!\
\
 Docelowo będą znajdować się tu wszelkie [wykłady](/lectures) (dwa zostały skopiowane z dawnej strony -- więcej dotrze kiedyś™), informacje o następnych i odbytych [zajęciach](/meetings) i przede wszystkim nasze zdjęcia -- ale nad tą ostatnią sekcją będzie jeszcze trochę pracy.