+++
title = "Wykład o kompozycji"
date = "2008-10-04T08:00:00+01:00"
author = "Jan Amborski"
cover = ""
tags = ["kompozycja", "technika", "wykład","podstawy"]
keywords = ["kompozcja", "technika", "wykład"]
description = "Podstawy kompozycji. Jeśli przegapiłæś pierwsze zajęcia, to zajrzyj koniecznie :)"
showFullContent = false
swohOther = false
+++


1. **Jak pokazać odbiorcom to o co nam chodziło w zdjęciu?**\
Za pomocą:\
  * światła (główny obiekt oświetlony- tło niedoświetlone, obiekt ciemny- tło prześwietlone)\
  * sztucznego tła (np. zdj. do paszportu)\
  * operacja ostrością (np. wszystko poza gł. obiektem jest nieostre)\
  * kompozycją

2. **Jakie są typy kompozycji?**\
  * ułożenie plam\
  * ułożenie granic między plamami

3. **Mocne punkty zdjęcia.**\
Dzielimy boki zdjęcia według złotego podziału. Przecięcie się linij wskazuje mocny punkt.

4. **Rodzaje kompozycji.**\
  * centralna\
  * po przekątnej (np. od lewego dolnego do górnego prawego)- jest najpopularniejsze i często\
powoduje iż zdjęcie jest bardziej dynamiczne.\
  * zdjęcie symetryczne

5. **Syndrom pliszki.**\
Jest to skrajna dysproporcja między tłem a obiektem fotografowanym.

6. **Niepokój wywołany dynamiką zdjęcia.**\
  * Niepokojem w fotografii nazywamy moment w którym patrzący nie wie co się dzieje na zdjęciu -- jego mózg nie odnajduje np. żadnego obiektu w mocnym punkcie i musi się zastanowić co w takim razie dzieje się na zdjęciu.\
  * Niepokój wywołany dynamiką zdjęcia można zobrazować na przykładzie zdjęcia biegacza -- jeśli zrobimy zdjęcie biegaczowi tak, że biegnie w kierunku środka zdjęcia, nie ma niepokoju wywołanego dynamiką zdjęcia. Jeśli jednak biegacz biegnie od środka, to nie wiemy dokąd zmierza i takie zdjęcie wywołuje w nas niepokój.

7. **Cykle**\
Dzięki cyklom zdjęć widać planowe działanie autora, gdyż dobre, pojedyńcze zdjęcie może być\
dziełem przypadku.

8. **Wystawa**\
Wystawa stanowi pewną całość, więc musi być otwarta jak i zamknięta jakimś zdjęciem -- pierwsze zdjęcie powinno kierować nasz wzrok w stronę następnego, a ostatnie powinno kierować wzrok do poprzedniego zdjęcia.\


Digitalizacja *Katarzyna Nurowska*, 2008\
Korekta *Bartłomiej Janowski*, 2019