+++
title = "Wykład o świetle"
date = "2008-10-31T08:00:00+01:00"
author = "Jan Amborski"
cover = ""
tags = ["światło", "technika", "wykład",  "balans bieli"]
keywords = ["światło", "technika", "wykład", "balans bieli"]
description = "Wykład prawi o użytkowanym w fotografii świetle -- wiedza dość konieczna dla chętnych do odwiedzin w studio."
showFullContent = false
swohOther = false
+++

**WYKŁAD O ŚWIETLE (czyli wprowadzenie do lamp błyskowych)**

1. **Ciało doskonale czarne** -- pojęcie powstałe dzięki fizykom. Jest to ciało całkowicie pochłaniające światło. W rzeczywistości nie istnieje, ale za jego przykład można wziąć czarną wnękę z małym otworem przez który wpada światło. Światło odbija sie pod różnymi kątami aż końcu ginie we wnęce. Ciało doskonale czarne emituje promieniowanie elektromagnetyczne -- np. światło o różnym kolorze (jak wsadzimy np. igłę pod palnik też się rozgrzewa i emituje czerwone, potem białe a na końcu niebieskie światło). Na podstawie prawa przesunięć Wiena możemy określić temperaturę ciała wiedząc jakim światłem ono świeci (tak np. określa się temperaturę gwiazd).

2. **Światło**, które dociera do nas, ma różną barwę. Nasze oko nie zauważa zazwyczaj tej różnicy (widzimy ją najczęściej przy wschodzie czy zachodzie słońca). Jednakże film zauważa te różnice. Najmniej czułymi na zmianę barwy światła filmami są filmy czarno-białe a najbardziej slajdy.

3. **Światło białe** ma temp. około 5500 stopni Kelwina. Wyższe temperatury dają światło bardziej niebieskie, niższe dają światło bardziej czerwone.

4. **Niektóre temperatury światła:**
  * 1500-2000 K -- światło świecy\
  * 2000-3000 K -- wschód słońca\
  * 2700 K -- żarówka wolframowa\
  * 5000-5500 K -- światło dzienne (okolice godziny 12)\
  * 7000 K -- świetlówka\
  * 7000-8000 K -- zachmurzone niebo , mgła\
  * 10000 K -- bezchmurne niebo

5. **Aby zdjęcie wyglądało normalnie **(nie było niebieskawe, żółtawe bądź czerwonawe) można zastosować jedną z poniższych metod:\
  * użyć specjalnego filmu\
  * zastosować filtr\
  * korekta po wykonaniu zdjęcia (przy wykonywaniu zdjęcia należy je trochę prześwietlić i pamiętać o stosowaniu tylko jednej barwy światła -- robimy przy lampie to zasłońmy okna aby nie wpadało słońce z zewnątrz) w photoshopie bądź ciemni (np. użyć foli korygującej). Folia korygująca powoduje powstanie pierścieni Newtona.\
  * dokonać korekty balansu bieli

  *Korekta -- Bartłomiej Janowski, 2019*