+++
title = "O nas"
+++

1. **Kim jesteśmy?**\
    Koło fotograficzne to cotygodniowe spotkanie, na którym robimy zdjęcia, rozmawiamy o zdjęciach i pracujemy ze zdjęciami. Ale nie tylko :) Zespół koła stanowią:\
    * [Jan Amborski](/people/j_amborski) -- *do uzupełnienia*
    * [Bartłomiej Janowski](/people/b_janowski) -- *do uzupełnienia*
    * [Michał K](/people/michal_k) -- *do uzupełnienia*

    Poza tymi osobami, na koło uczęszczają:

    * [Osoba 1](/) -- *do uzupełnienia*
    * [Osoba 2](/) -- *do uzupełnienia* 
    * [Osoba 3](/) -- *do uzupełnienia*

2. **Gdzie pracujemy?**\
   Spotykamy się przede wszysktim w [Ciemni](/pages/ciemnia) (s. 222). Poza tym dyspoujemy pomieszczeniami w piwnicy pod pawilonem, ale realizujemy też plenery w i poza szkołą.
3. **Co chcemy osiągnąć?**\
   Długofalowym celem Koła jest stopniowa wymiana fotografii, które zdobią staszicowe korytarze -- te jak wiemy są miejscami już dość wiekowe. Rok 2019/2020 jest kolejnym w którym planujemy zrealizować wystawę -- może w końcu się uda :)