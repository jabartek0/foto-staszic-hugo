+++
title = "Kiedy jest koło?"
+++


Koło odbywa się w **środy** o godz. 15:10 w [Ciemni](/pages/ciemnia) (s. 222) w XIV LO im. Stanisława Staszica w Warszawie.\
======

Wszelkie odstępstwa od tego terminu będą publikowane na stronie :)\
***
Adres szkoły:\
XIV LO im. Stanisława Staszica w Warszawie\
Ul. Nowowiejska 37a\
02-776 Warszawa